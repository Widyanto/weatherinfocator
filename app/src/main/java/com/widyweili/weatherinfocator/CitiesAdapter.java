package com.widyweili.weatherinfocator;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.widyweili.weatherinfocator.ui.main.CityDetailsFragment;
import com.widyweili.weatherinfocator.weathercontent.Coord;

import java.util.HashMap;

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.CityViewHolder> {

    private Context context;
    private HashMap<String, Coord> cityLocations;
    private String[] cities;

    public CitiesAdapter(String[] newCities, HashMap<String,Coord> newCityLocations, Context c){
        cities = newCities;
        cityLocations = newCityLocations;
        context = c;
    }

    @NonNull
    @Override
    public CityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_city,parent,false);
        Log.d("test",String.valueOf(v));
        return new CityViewHolder((v));
    }

    @Override
    public void onBindViewHolder(@NonNull CityViewHolder holder, int position) {
        holder.cityName.setText(cities[position]);
    }

    @Override
    public int getItemCount() {
        return cities.length;
    }

    public class CityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView cityName;
        public CityViewHolder(@NonNull View itemView) {
            super(itemView);
            cityName = itemView.findViewById(R.id.city_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            String selectedCity = cities[pos];
            float lat = cityLocations.get(selectedCity).getLat();
            float lon = cityLocations.get(selectedCity).getLon();
            CityDetailsFragment cdf = new CityDetailsFragment(selectedCity,lat,lon);
            FragmentManager fragmentManager = ((MainActivity)context).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, cdf);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }
}
