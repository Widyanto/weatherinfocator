package com.widyweili.weatherinfocator.ui.main;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.widyweili.weatherinfocator.weathercontent.IWeatherData;
import com.widyweili.weatherinfocator.R;
import com.widyweili.weatherinfocator.weathercontent.Current;
import com.widyweili.weatherinfocator.weathercontent.Hourly;
import com.widyweili.weatherinfocator.weathercontent.WeatherData;
import com.widyweili.weatherinfocator.weathercontent.WeatherThreshold;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// some useful link for understanding utc format: https://www.unixtimestamp.com/index.php
public class CityDetailsFragment extends Fragment {
    private HashMap<String, WeatherThreshold> weatherThresholds = new HashMap<String,WeatherThreshold>(){{
        put("Temperature", new WeatherThreshold(0,40));
        put("Clouds", new WeatherThreshold(0,100));
        put("Humidity",new WeatherThreshold(70,100));
        put("Pressure",new WeatherThreshold(1000,1090));
        put("Wind Degree", new WeatherThreshold(0,360));
        put("Wind Speed", new WeatherThreshold(0,10));
    }
    };
    private List<Hourly> forecastHourly = new ArrayList<>();
    private List<Hourly> prevWeatherHourly = new ArrayList<>();

    private String selectedCityName;
    private static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    private static final String UNITS = "metric";
    private static final String LANG = "id";
    private static final String APP_ID = "04ae83af63819ecd878780688a2efd85";

    // xmls
    private TextView prevWeatherInfo;
    private TextView forecastInfo;
    private TextView temp;
    private TextView clouds;
    private TextView humidity;
    private TextView pressure;
    private TextView winddir;
    private TextView windspd;

    //graph part
    private AdapterView.OnItemSelectedListener forecastSpinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String key = parent.getItemAtPosition(position).toString();
            String forecastStr = "Forecast for 48 hours(x in Hours, y in "+key+")";
            WeatherThreshold wtRef = weatherThresholds.get(key);
            setupGraphData(forecastGraph,forecastHourly,false,key);
            forecastInfo.setText(forecastStr);
            forecastGraph.getViewport().setMinY(wtRef.getMin());
            forecastGraph.getViewport().setMaxY(wtRef.getMax());
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
    private AdapterView.OnItemSelectedListener prevWeatherSpinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String key = parent.getItemAtPosition(position).toString();
            int totalData = prevWeatherHourly.size();
            String forecastStr = "Previous weather for "+totalData+" hours(x in Hours, y in "+key+")";
            WeatherThreshold wtRef = weatherThresholds.get(key);
            setupGraphData(previousWeatherGraph,prevWeatherHourly,true,key);
            prevWeatherInfo.setText(forecastStr);
            previousWeatherGraph.getViewport().setMinY(wtRef.getMin());
            previousWeatherGraph.getViewport().setMaxY(wtRef.getMax());
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private GraphView forecastGraph;
    private Spinner forecastSpinner;
    private GraphView previousWeatherGraph;
    private Spinner prevWeatherSpinner;

    public CityDetailsFragment(String newSelectedCityName, float newLat, float newLon){
        selectedCityName = newSelectedCityName;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IWeatherData iwd = retrofit.create(IWeatherData.class);
        Call<WeatherData> weatherDataCall = iwd.getWeatherDatas(newLat,newLon,UNITS,LANG,APP_ID);

        weatherDataCall.enqueue(new Callback<WeatherData>() {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                if(response.code() == 404 || !response.isSuccessful()){
                    Toast.makeText(getContext(),"Error on getting forecast weather", Toast.LENGTH_LONG).show();
                    return;
                }
                WeatherData wd = response.body();

                Current c = wd.getCurrent();
                visualizeData(temp,c.getTemp(),"°C");
                visualizeData(clouds,c.getClouds(),"%");
                visualizeData(humidity,c.getHumidity(),"%");
                visualizeData(pressure,c.getPressure(),"hPa");
                visualizeData(winddir,c.getWindDeg(),"°");
                visualizeData(windspd,c.getWindSpeed(),"m/s");

                forecastHourly = wd.getHourly();
                setupGraphData(forecastGraph,forecastHourly,false, "Temperature");
            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {

            }
        });

        final Calendar cal = Calendar.getInstance();
        int curSecMinThirty = Calendar.getInstance().get(Calendar.SECOND)-1800; // reduce current time by half hour
        cal.set(Calendar.SECOND,curSecMinThirty);
        long toSecond = cal.getTimeInMillis()/1000;
        Call<WeatherData> prevWeatherCall = iwd.getPrevWeatherDatas(newLat,newLon,toSecond,UNITS,LANG,APP_ID);

        prevWeatherCall.enqueue(new Callback<WeatherData>() {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                if(response.code() == 404 || !response.isSuccessful()){
                    Toast.makeText(getContext(),"Error on getting previous weather", Toast.LENGTH_LONG).show();
                    return;
                }

                WeatherData wd = response.body();
                prevWeatherHourly = wd.getHourly();
                int totalData = prevWeatherHourly.size();
                String forecastStr = "Previous weather for "+totalData+" hours(x in Hours, y in Temperature)";
                prevWeatherInfo.setText(forecastStr);
                setupGraphData(previousWeatherGraph,prevWeatherHourly,true, "Temperature");
            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_city_details, container, false);

        TextView cityName = v.findViewById(R.id.city_name_details);
        cityName.setText(selectedCityName);

        prevWeatherInfo = v.findViewById(R.id.prev_weather_info);
        forecastInfo = v.findViewById(R.id.forecast_info);
        temp = v.findViewById(R.id.temp);
        clouds = v.findViewById(R.id.clouds);
        humidity = v.findViewById(R.id.humidity);
        pressure = v.findViewById(R.id.pressure);
        winddir = v.findViewById(R.id.wind_direction);
        windspd = v.findViewById(R.id.wind_speed);

        //graph setup
        forecastSpinner = v.findViewById(R.id.forecast_spinner);
        prevWeatherSpinner = v.findViewById(R.id.prev_weather_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.weatherdata, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        forecastSpinner.setAdapter(adapter);
        forecastSpinner.setSelection(0);
        forecastSpinner.setOnItemSelectedListener(forecastSpinnerListener);

        prevWeatherSpinner.setAdapter(adapter);
        prevWeatherSpinner.setSelection(0);
        prevWeatherSpinner.setOnItemSelectedListener(prevWeatherSpinnerListener);

        forecastGraph = v.findViewById(R.id.graph_forecast_48hours);
        previousWeatherGraph = v.findViewById(R.id.graph_prev_24hours);
        setupGraph(forecastGraph,true);
        setupGraph(previousWeatherGraph,false);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        forecastSpinner.setOnItemSelectedListener(null);
        forecastSpinner.setAdapter(null);
        prevWeatherSpinner.setOnItemSelectedListener(null);
        prevWeatherSpinner.setAdapter(null);
    }

    private void setupGraph(GraphView graphRef, boolean isSettingForecast){
        graphRef.getViewport().setYAxisBoundsManual(true);
        graphRef.getViewport().setMinY(0);
        graphRef.getViewport().setMaxY(40);

        int minX = isSettingForecast?-1:-24;
        int maxX = isSettingForecast?46:0;
        graphRef.getViewport().setXAxisBoundsManual(true);
        graphRef.getViewport().setMinX(minX);
        graphRef.getViewport().setMaxX(maxX);

        graphRef.getViewport().setScrollable(true);
        graphRef.getViewport().setScrollableY(true);
        graphRef.getViewport().setScalable(true);
        graphRef.getViewport().setScalableY(true);
    }

    private void setupGraphData(GraphView graphRef, List<Hourly> hoursRef, boolean reverseX, String spinnerKey){
        LineGraphSeries series = new LineGraphSeries<>();
        series.setColor(Color.GREEN);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(3);
        series.setThickness(2);
        graphRef.removeAllSeries();

        int totalSize = hoursRef.size();
        int hourOffset = !reverseX? -1:-totalSize;
        float refVal;
        for(int i = 0;i<totalSize;i++){
            Hourly hourRef = hoursRef.get(i);
            switch (spinnerKey){
                case "Temperature":
                    refVal = hourRef.getTemp();
                    break;
                case "Clouds":
                    refVal = hourRef.getClouds();
                    break;
                case "Humidity":
                    refVal = hourRef.getHumidity();
                    break;
                case "Pressure":
                    refVal = hourRef.getPressure();
                    break;
                case "Wind Degree":
                    refVal = hourRef.getWindDeg();
                    break;
                    default:
                        refVal = hourRef.getWindSpeed();
                        break;
            }
            DataPoint newDataPoint = new DataPoint(hourOffset,refVal);
            series.appendData(newDataPoint,true,totalSize+1);
            hourOffset++;
        }
        graphRef.addSeries(series);
    }

    private void visualizeData(TextView refText, float dataRef, String ext){
        String tempStr = dataRef+ext;
        refText.setText(tempStr);
    }
}
