package com.widyweili.weatherinfocator.ui.main;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.widyweili.weatherinfocator.CitiesAdapter;
import com.widyweili.weatherinfocator.weathercontent.Coord;
import com.widyweili.weatherinfocator.R;

import java.util.ArrayList;
import java.util.HashMap;

public class CityListFragment extends Fragment {
    private RecyclerView citiesRV;
    private CitiesAdapter citiesAdapter;

    private HashMap<String, Coord> cityLocations = new HashMap<String,Coord>(){{
        put("Jakarta", new Coord(106.845131f,-6.21462f));
        put("Surabaya", new Coord(108.020897f, -7.0259f));
        put("Medan", new Coord(98.666672f, 3.58333f));
        put("Bandung", new Coord(107.618607f,-6.90389f));
        put("Makassar", new Coord(119.422096f, -5.14f));
        put("Semarang", new Coord(110.420303f, -6.9932f));
        put("Palembang", new Coord(104.745796f,-2.91673f));
        put("Pekanbaru", new Coord(101.449997f,0.53333f));
        put("Malang", new Coord(112.630402f,-7.9797f));
    }
    };

    private ArrayList<String> arrCities = new ArrayList<String>(){
        {
            add("Jakarta");
            add("Surabaya");
            add("Medan");
            add("Bandung");
            add("Makassar");
            add("Semarang");
            add("Palembang");
            add("Pekanbaru");
            add("Malang");
        }
    };

    private String[] cities = new String[]{
            "Jakarta","Surabaya","Medan","Bandung","Makassar","Semarang","Palembang","Pekanbaru","Malang"
    };

    public static CityListFragment newInstance() {
        return new CityListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.city_list_fragment, container,false);

        citiesAdapter = new CitiesAdapter(cities, cityLocations, getContext());
        citiesRV = v.findViewById(R.id.recycler_view_cities);
        citiesRV.setLayoutManager(new LinearLayoutManager(getContext()));
        citiesRV.setAdapter(citiesAdapter);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        citiesRV.setAdapter(null);
    }
}
