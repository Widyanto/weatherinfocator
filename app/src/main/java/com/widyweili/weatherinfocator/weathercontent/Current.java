
package com.widyweili.weatherinfocator.weathercontent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Current {

    @SerializedName("dt")
    @Expose
    private long dt;
    @SerializedName("temp")
    @Expose
    private float temp;
    @SerializedName("pressure")
    @Expose
    private int pressure;
    @SerializedName("humidity")
    @Expose
    private int humidity;
    @SerializedName("clouds")
    @Expose
    private int clouds;
    @SerializedName("wind_speed")
    @Expose
    private float windSpeed;
    @SerializedName("wind_deg")
    @Expose
    private int windDeg;

    public long getDt() {
        return dt;
    }
    public float getTemp() {
        return temp;
    }
    public int getPressure() {
        return pressure;
    }
    public int getHumidity() {
        return humidity;
    }
    public int getClouds() {
        return clouds;
    }
    public float getWindSpeed() {
        return windSpeed;
    }
    public int getWindDeg() {
        return windDeg;
    }

}
