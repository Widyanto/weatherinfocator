
package com.widyweili.weatherinfocator.weathercontent;

public class Coord {
    private float lon;
    private float lat;

    public Coord(float newLon, float newLat){
        lon = newLon;
        lat = newLat;
    }

    public float getLon() {
        return lon;
    }
    public float getLat() {
        return lat;
    }
}
