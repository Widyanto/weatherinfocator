
package com.widyweili.weatherinfocator.weathercontent;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeatherData {

    @SerializedName("current")
    @Expose
    private Current current;
    @SerializedName("hourly")
    @Expose
    private List<Hourly> hourly = null;

    public Current getCurrent() {
        return current;
    }
    public List<Hourly> getHourly() {
        return hourly;
    }
}
