package com.widyweili.weatherinfocator.weathercontent;

// this class is used for setting threshold of a graphview(for visual only)
public class WeatherThreshold {
    private int min;
    private int max;

    public WeatherThreshold(int newMin, int newMax){
        min = newMin;
        max = newMax;
    }

    public int getMin(){return min;}
    public int getMax(){return max;}
}
