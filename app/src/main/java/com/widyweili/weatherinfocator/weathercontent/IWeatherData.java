package com.widyweili.weatherinfocator.weathercontent;

import com.widyweili.weatherinfocator.weathercontent.WeatherData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IWeatherData {
    // get forecast for 48 hours
    @GET("onecall")
    Call<WeatherData> getWeatherDatas(@Query("lat") float lat,
                                      @Query("lon") float lon,
                                      @Query("units") String units,
                                      @Query("lang") String languange,
                                      @Query("appid") String appid);

    // get previous weather for a day
    @GET("onecall/timemachine")
    Call<WeatherData> getPrevWeatherDatas(@Query("lat") float lat,
                                          @Query("lon") float lon,
                                          @Query("dt") long dt,
                                          @Query("units") String units,
                                          @Query("lang") String languange,
                                          @Query("appid") String appid);
}
